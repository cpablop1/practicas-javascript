'use strict'

let args = process.argv.slice(2);

let number1 = parseFloat(args[0]);
let number2 = parseFloat(args[1]);

let layout = `
    La suma es: ${number1 + number2}
    La resta es: ${number1 - number2}
    La multiplicación es: ${number1 * number2}
    La división es: ${(number1 / number2).toFixed(2)}
`;

console.log(layout);