'use strict'

let Proyect = require('../models/projects');

let controller = {
    home: (req, res) => {
        return res.status(200).send({
            message: 'Home page...'
        });
    },
    test: (req, res) => {
        console.log('\nRoute test\n');
        return res.status(200).send({
            message: 'I method of project controller'
        });
    },
    saveProject: async (req, res) => {
        try {
            let project = new Proyect();
            let params = req.body;

            project.name = params.name;
            project.description = params.description;
            project.category = params.category;
            project.year = params.year;
            project.langs = params.langs;
            project.image = null;

            let projectStored = await project.save();

            return res.status(200).send({ project: projectStored });
        } catch (error) {
            console.log(error);
            return res.status(500).send({ message: 'Error al guardar el documento!' });
        }
    },
    getProject: async (req, res) => {
        try {
            let projectId = req.params.id;
            if (projectId == null) return res.status(404).send('No hay datos que buscar.');
            let project = await Proyect.findById(projectId);
            return res.status(200).send({ project: project })
        } catch (error) {
            res.status(404).send('Error al buscar datos...');
        }
    },
    getProjects: (req, res) => {
        try {
            Proyect.find().exec().then(projects => {
                if (projects.length == 0) return res.status(404).send({ message: 'No hay proyectos que mostrar...' })
                return res.status(200).send({ projects: projects });
            }).catch(error => {
                console.log('\nPromise error', error);
            });
        } catch (error) {
            return res.status(500).send({ message: 'Error al listar proyectos!' });
        }
    },
    updateProject: (req, res) => {
        try {
            let projectId = req.params.id;
            const update = req.body;
            Proyect.findByIdAndUpdate(projectId, update, { new: true }).then((projectUpdated) => {
                console.log(projectUpdated);
                res.status(200).send({ project: projectUpdated });
            }).catch(error => {
                console.log('\nNo existe ningún documento con dicho ID!');
                res.status(404).send({ message: 'No existe ningún documento con dicho ID!' });
            });
        } catch (error) {
            console.log('\nError en servidor...');
        }
    },
    deleteProject: (req, res) => {
        let projectId = req.params.id;
        try {
            Proyect.findByIdAndDelete(projectId).exec().then(projectRemoved => {
                console.log(projectRemoved);
                res.status(200).send({ project: projectRemoved });
            }).catch(error => {
                console.log('\nNo existe documento con dicho ID!');
                res.status(404).send({ message: 'No existe documento con dicho ID!' });
            });
        } catch (error) {
            res.status(500).send({ message: 'Ocurrió un error en el servidor!' });
        }
    },
    uploadImage: (req, res) => {
        try {
            let projectId = req.params.id;

            if (req.files) {
                let filePath = req.files.image.path;
                let fileSplit = filePath.split('\\');
                let fileName = fileSplit[1]
                console.log(req.files);
                Proyect.findByIdAndUpdate(projectId, { image: fileName }, { new: true }).then(projectUpdated => {
                    return res.status(200).send({ project: projectUpdated });
                }).catch(error => {
                    return res.status(404).send({ message: 'No se pudo actualizar documento' });
                });
            } else {
                return res.status(404).send({ message: 'No hay archivo que cargar...' });
            }
        } catch (error) {
            res.status(500).send({ message: 'Hubo un error en el servidor!' });
        }
    }
}

module.exports = controller;