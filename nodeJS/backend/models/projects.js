'use strict'

let mongoose = require('mongoose');
let schema = mongoose.Schema;

let ProjectSchema = schema({
    name: String,
    description: String,
    category: String,
    year: Number,
    langs: String,
    image: String
});

module.exports = mongoose.model('Projects',ProjectSchema);