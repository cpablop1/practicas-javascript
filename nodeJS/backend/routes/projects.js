'use strict'

const express = require('express');
const ProjectController = require('../controllers/projects');

const router = express.Router({ caseSensitive: true });

const multipart = require('connect-multiparty');
const multiplartMiddleware = multipart({ uploadDir: './uploads' });

router.get('/home', ProjectController.home);
router.get('/test', ProjectController.test);
router.get('/save-project', ProjectController.saveProject);
router.get('/project/:id?', ProjectController.getProject);
router.get('/projects', ProjectController.getProjects);
router.put('/project/:id', ProjectController.updateProject);
router.delete('/project/:id', ProjectController.deleteProject);
router.post('/upload-image/:id', multiplartMiddleware, ProjectController.uploadImage);

module.exports = router;