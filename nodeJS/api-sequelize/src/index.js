const app = require('./app/app.js');
const startDb = require('./model/start.db.js');
const port = process.env.PORT || 3001;
app.listen(port, async () => {
  console.log('Running server...');

  /* await startDb().drop();
  console.log('All deleted tables...'); */

  Promise.all([
    //sequelizeInstanceOne.sync()
    startDb().sync({ alter: true })
      .then(() => {
        console.log('Tables for the AUTHENTICATION AREA created successfully, FROM FIRST INSTANCE!');
      }).catch((error) => {
        console.error('Error into the sync the DB: ', error);
      })
  ]);
});