const routerCat = require('express').Router();
const { faker } = require('@faker-js/faker');
const Category = require('../model/category.model.js');

routerCat.get('/category', async (req, res) => {
    const categories = await Category.findAll();
    res.status(200).json({
        ok: true,
        status: 200,
        body: categories
    });
});
routerCat.get('/category/:categoryId', async (req, res) => {
    const id = req.params.categoryId;
    const categories = await Category.findOne({
        where: {
            categoryId: id
        }
    });
    res.status(200).json({
        ok: true,
        status: 200,
        body: categories
    });
});
routerCat.post('/category', async (req, res) => {
    const dataCategories = req.body;
    await Category.sync();
    const createCategory = await Category.create({
        categoryName: dataCategories.categoryName,
        categoryDescr: dataCategories.categoryDescr
    });
    res.status(201).json({
        ok: true,
        status: 201,
        message: 'Created Category'
    });
});
routerCat.put('/category/:categoryId', async (req, res) => {
    const id = req.params.categoryId;
    const dataCategories = req.body;
    await Category.sync();
    const updateCategory = await Category.update({
        categoryName: dataCategories.categoryName,
        categoryDescr: dataCategories.categoryDescr
    },
    {
        where: {categoryId: id}
    });
    res.status(201).json({
        ok: true,
        status: 201,
        message: 'Updated Category'
    });
});
routerCat.delete('/category/:categoryId', async (req, res) => {
    const id = req.params.categoryId;
    const deleteCategory = await Category.destroy({
        where: {categoryId: id}
    });
    console.log('Deleted category... ', deleteCategory);
    res.status(200).json({
        ok: true,
        status: 200,
        body: deleteCategory
    });
});

module.exports = routerCat;