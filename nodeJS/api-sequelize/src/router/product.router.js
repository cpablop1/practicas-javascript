const routerPro = require('express').Router();
const { faker } = require('@faker-js/faker');
const Products = require('../model/product.model.js');

routerPro.get('/products', async (req, res) => {
    const products = await Products.findAll();
    res.status(200).json({
        ok: true,
        status: 200,
        body: products
    });
});
routerPro.get('/products/:product_id', async (req, res) => {
    const id = req.params.product_id;
    const product = await Products.findOne({
        where: {
            product_id: id
        }
    });
    res.status(200).json({
        ok: true,
        status: 200,
        body: product
    });
});
routerPro.post('/products', async (req, res) => {
    const dataProducts = req.body;
    let createProduct = '';
    let ok = true;
    let status = 200
    let message;
    try {
        await Products.sync();
        createProduct = await Products.create({
            product_name: dataProducts.product_name,
            price: dataProducts.price,
            is_stock: dataProducts.is_stock,
            category_id: dataProducts.category_id,
            stock: dataProducts.stock
        });
        message = 'Created Product';
    } catch(error){
        createProduct = error.errors[0].message;
        ok = false;
        status = 404
        message = 'Created Product failed!'
    }
    /* const createProduct = await Products.create({
        product_name: faker.commerce.product(),
        price: faker.commerce.price(),
        is_stock: faker.datatype.boolean()
    }); */
    res.status(201).json({
        ok: ok,
        status: status,
        message: message,
        product: createProduct
    });
});
routerPro.put('/products/:product_id', async (req, res) => {
    const id = req.params.product_id;
    const dataProducts = req.body;
    await Products.sync();
    const updateProduct = await Products.update({
        product_name: dataProducts.product_name,
        price: dataProducts.price,
        is_stock: dataProducts.is_stock,
        category_id: dataProducts.category_id
    },
    {
        where: {product_id: id}
    });
    res.status(201).json({
        ok: true,
        status: 201,
        message: 'Updated Product'
    });
});
routerPro.delete('/products/:product_id', async (req, res) => {
    const id = req.params.product_id;
    const deleteProduct = await Products.destroy({
        where: {product_id: id}
    });
    console.log('Deleted product... ', deleteProduct);
    res.status(200).json({
        ok: true,
        status: 200,
        body: deleteProduct
    });
});

module.exports = routerPro;