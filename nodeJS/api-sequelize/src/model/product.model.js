/* const { Sequelize, DataTypes, Model } = require('sequelize');
const sequelize = new Sequelize('product_test', 'root', '', {
    host: 'localhost',
    dialect: 'mysql',
    port: 3306
}); */
const { DataTypes, Model } = require('sequelize');
const startDb = require("./start.db");
const Category = require('./category.model');

class Product extends Model { }

Product.init(
  {
    product_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    product_name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    price: {
      type: DataTypes.FLOAT(10, 2),
      allowNull: false
    },
    is_stock: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    stock: {
      type: DataTypes.INTEGER,
      validate: {
        isInt: {
          msg: 'Stock must be an integer...'
        }
      }
    },
    category_id: {
      type: DataTypes.INTEGER,
      references: {
        model: Category,
        key: 'category_id'
      }
    }
  },
  {
    sequelize: startDb(),
    modelName: 'Product',
    logging: false,
    timestamps: false
  }
);

Product.belongsTo(Category, { foreignKey: 'category_id' });

module.exports = Product;

/* async function testConnection() {
    try {
        await sequelize.authenticate();
        console.log('All Good!!');
    } catch (error) {
        console.log('Connection error...!', error);
    }
}

testConnection(); */