
const { DataTypes, Model } = require('sequelize');
const startDb = require("./start.db");

class Category extends Model { }

Category.init(
  {
    categoryId: {
      field: 'category_id',
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    categoryName: {
      field: 'category_name',
      type: DataTypes.STRING(200),
      allowNull: false
    },
    categoryDescr: {
      field: 'category_descr',
      type: DataTypes.STRING(200),
      allowNull: false
    },
  },
  {
    sequelize: startDb(),
    modelName: 'Category',
    logging: false,
    timestamps: false
  }
);

module.exports = Category;