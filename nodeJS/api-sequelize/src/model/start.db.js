const { Sequelize } = require('sequelize');

function startDb() {
  const sequelize = new Sequelize('product_test', 'root', '', {
    host: 'localhost',
    dialect: 'mysql',
    port: 3306,
    logging: false
  });

  return sequelize;
}

module.exports = startDb