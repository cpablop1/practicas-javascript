const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const routerPro = require('../router/product.router.js');
const routerCat = require('../router/category.router.js');
const app = express()

app.use(morgan('dev'));
app.get('/', (req, res) => {
    res.send('This is Express...');
});

//app.use(express.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use('/api/v2', routerCat);
app.use('/api/v1', routerPro);

module.exports = app;