> # Node.js ![](https://icon-library.com/images/nodejs-icon/nodejs-icon-17.jpg){width=100px}

Node.js es un entorno de ejecución de JavaScript multiplataforma y de código abierto. ¡Es una herramienta popular para casi cualquier tipo de proyecto!

Node.js ejecuta el motor JavaScript V8, el núcleo de Google Chrome, fuera del navegador. Esto permite que Node.js tenga un gran rendimiento.

Una aplicación Node.js se ejecuta en un único proceso, sin crear un nuevo hilo para cada solicitud. Node.js proporciona un conjunto de primitivas de E/S asíncronas en su biblioteca estándar que evitan que el código JavaScript se bloquee y, en general, las bibliotecas en Node.js se escriben utilizando paradigmas sin bloqueo, lo que hace que el comportamiento de bloqueo sea la excepción y no la norma.

Cuando Node.js realiza una operación de E/S, como leer desde la red, acceder a una base de datos o al sistema de archivos, en lugar de bloquear el subproceso y desperdiciar ciclos de CPU esperando, Node.js reanudará las operaciones cuando llegue la respuesta.

Esto permite a Node.js manejar miles de conexiones simultáneas con un único servidor sin introducir la carga de administrar la concurrencia de subprocesos, lo que podría ser una fuente importante de errores.

Node.js tiene una ventaja única porque millones de desarrolladores frontend que escriben JavaScript para el navegador ahora pueden escribir el código del lado del servidor además del código del lado del cliente sin la necesidad de aprender un idioma completamente diferente.

En Node.js los nuevos estándares ECMAScript se pueden utilizar sin problemas, ya que no tienes que esperar a que todos tus usuarios actualicen sus navegadores: tú eres el encargado de decidir qué versión de ECMAScript usar cambiando la versión de Node.js. y también puede habilitar funciones experimentales específicas ejecutando Node.js con banderas.

## Un ejemplo de aplicación Node.js
El ejemplo más común de Hello World de Node.js es un servidor web:

```
const http = require('node:http');
const hostname = '127.0.0.1';
const port = 3000;
const server = http.createServer((req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
  res.end('Hello World\n');
});
server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});
```
Para ejecutar este fragmento, guárdelo como un `server.js` archivo y ejecútelo `node server.js` en su terminal.

Este código incluye primero el `http` [módulo](https://nodejs.org/api/http.html) Node.js.

[Node.js tiene una biblioteca estándar](https://nodejs.org/api/) fantástica , que incluye soporte de primera clase para redes.

El `createServer()` método `http` crea un nuevo servidor HTTP y lo devuelve.

El servidor está configurado para escuchar en el puerto y nombre de host especificados. Cuando el servidor está listo se llama a la función de devolución de llamada, en este caso informándonos que el servidor se está ejecutando.

Cada vez que se recibe una nueva solicitud, se llama al `request` [evento](https://nodejs.org/api/http.html#http_event_request) , que proporciona dos objetos: una solicitud (un `http.IncomingMessage` objeto) y una respuesta (un `http.ServerResponse` objeto).

Esos 2 objetos son esenciales para manejar la llamada HTTP.

El primero proporciona los detalles de la solicitud. En este ejemplo sencillo, esto no se utiliza, pero se puede acceder a los encabezados y datos de la solicitud.

El segundo se utiliza para devolver datos a la persona que llama.

En este caso con:

```
res.statusCode = 200;
```
Establecemos la propiedad statusCode en 200, para indicar una respuesta exitosa.

Configuramos el encabezado Content-Type:

```
res.setHeader('Content-Type', 'text/plain');
```
y cerramos la respuesta, agregando el contenido como argumento a end():

```
res.end('Hello World\n');
```
