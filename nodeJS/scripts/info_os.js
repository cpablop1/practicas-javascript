const os = require('node:os');

console.log('Info del SO: ', os.platform());
console.log('Versión del SO: ', os.release());
console.log('Arquitectura del SO: ', os.arch());
console.log('Infor CPUs: ', os.cpus());
console.log(os.version());