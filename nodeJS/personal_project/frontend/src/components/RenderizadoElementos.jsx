import React, { Component } from "react";
import data from "../herpers/data.json";

function ElementoLista(props) {
    return (
        <li>
            <a href={props.value.web} target="_blank">{props.value.name}</a>
        </li>
    );
}

export default class RenderizadoElementos extends Component {
    constructor(props) {
        super(props);
        this.state = {
            seasons: ['Primaver', 'Verano', 'Otoño', 'Invierno']
        }
    }
    render() {
        return (
            <div>
                <h2>Renderizado Elementos</h2>
                <h3>Estaciones del año</h3>
                <ol>
                    {this.state.seasons.map((item, index) => (
                        <li key={index}>{item}</li>
                    ))}
                </ol>
                <h3>Frameworks Frontend JavaScript</h3>
                <ol>
                    {
                        data.frameworks.map((value, index)=><ElementoLista value={value} key={value.id} />)
                    }
                </ol>
            </div>
        );
    }
}