import React, { Component } from "react";

function EstadoHijo(props){
    return(
        <div>
            <h2>{props.contadorHijo}</h2>
        </div>
    );
}

export default class Estado extends Component {
    constructor(props){
        super(props);
        this.state = {
            contador: 0,
        };

        /* setInterval(() => {
            this.setState({
                contador: this.state.contador + 1
            });
        }, 1000); */
    }
    render() {
        return (
            <div>
                <h2>El estado</h2>
                <h2>{this.state.contador}</h2>
                <EstadoHijo contadorHijo={this.state.contador} />
            </div>
        );
    }
}