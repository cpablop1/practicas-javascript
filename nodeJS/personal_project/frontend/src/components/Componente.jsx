import React, { Component } from "react";

const Componente = props => {
    return (
        <>
            <h2>This is a new component</h2>
            <h2>{props.msg}</h2>
        </>
    )
}

/* function Componente(props) {
    return (
        <>
            <h2>This is a new component</h2>
            <h2>{props.msg}</h2>
        </>
    ) 
} */

/* class Componente extends Component {
    render(args) {
        return <h2>That is new component</h2>
    }
} */

export default Componente;