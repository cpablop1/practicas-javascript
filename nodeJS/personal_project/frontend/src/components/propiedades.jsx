import PropTypes from 'prop-types'

export default function Propiedades(props) {
    return (
        <>
            <h2>Property from function</h2>
            <h2>{props.msg}</h2>
            <h2>{props.defaultProps1}</h2>
            <h2>{props.defaultProps2}</h2>
            <h2>{props.number}</h2>
            <h2>{props.name}</h2>
        </>
    )
}

Propiedades.defaultProps = {
    defaultProps1: 'Default property 1',
    defaultProps2: 'Default property 2'
}

Propiedades.propTypes = {
    number: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired
}