import React, { Component } from "react";

export class EventosES6 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            contador: 0
        };
        this.sumar = this.sumar.bind(this);
        this.restar = this.restar.bind(this);
    }

    sumar() {
        console.log('Sumando...');
        this.setState({
            contador: this.state.contador + 1
        });
    }

    restar() {
        console.log('Restando...');
        this.setState({
            contador: this.state.contador - 1
        });
    }

    render() {
        return (
            <div>
                <h2>Eventos de Componentes de Clase ES6</h2>
                <nav>
                    <button onClick={this.sumar}>➕</button>
                    <button onClick={this.restar}>➖</button>
                </nav>
                <h3>{this.state.contador}</h3>
            </div>
        );
    }
}

export class EventosES7 extends Component {
    state = {
        contador: 0
    };

    // Arrow functions
    sumar = () => {
        console.log('Sumando...');
        this.setState({
            contador: this.state.contador + 1
        });
    }

    restar = () => {
        console.log('Restando...');
        this.setState({
            contador: this.state.contador - 1
        });
    }

    render() {
        return (
            <div>
                <h2>Eventos de Componentes de Clase ES7</h2>
                <nav>
                    <button onClick={this.sumar}>➕</button>
                    <button onClick={this.restar}>➖</button>
                </nav>
                <h3>{this.state.contador}</h3>
            </div>
        );
    }
}

/* function Boton(props) {
    return <button onClick={props.MyOnClick}>Botón hecho con componente</button>
} */

/* let Boton = (props) => (<button onClick={props.MyOnClick}>Botón hecho con componente</button>) */

let Boton = ({MyOnClick}) => (<button onClick={MyOnClick}>Botón hecho con componente</button>)

export class MasSobreEventos extends Component {
    handleClick = (e, msg) => {
        console.log(e);
        console.log(msg);
    }
    render () {
        return(
            <div>
                <h2>Más sobre eventos en React JS</h2>
                {/* <button onClick={this.handleClick}>Saludar</button> */}
                <button onClick={(e) => this.handleClick(e, 'Hola, soy un arg adicional...')}>Saludar</button> {// En es manejador de eventos
                    // nos sirve para pasar parametros adicionales
                }
                <Boton MyOnClick={(e) => this.handleClick(e, 'Hola, soy un arg adicional...')}/>
            </div>
        );
    }
}