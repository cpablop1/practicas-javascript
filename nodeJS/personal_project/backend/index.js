'use strict'
const app = require('./app.js');
const port = 3700;
const Connection = require('./config/connection.js');
const connected = new Connection();

connected.connect().then(() => {
    console.log('\nSuccessful connection to the database.');
    // Servidor creation
    app.listen(port, () => {
        console.log('Server running correctly at url --> localhost:3700\n');
    });
}).catch(error => {
    console.log('\nThere was an error connecting to the database!\n');
});