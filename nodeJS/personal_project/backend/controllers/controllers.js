'use strict'
const Model = require('../models/models.js');

class Controller {
    constructor() {
        this.model = new Model();
    }
    home = (request, response) => {
        return response.status(200).send({ message: 'This is the home page.' });
    }
    create = (request, response) => {
        const people = {
            first_name: request.body.first_name,
            last_name: request.body.last_name,
            phone: request.body.phone,
            email: request.body.email
        };
        this.model.create(people).then(message => {
            response.status(200).send({ message: message });
        });
    }
}

module.exports = Controller;