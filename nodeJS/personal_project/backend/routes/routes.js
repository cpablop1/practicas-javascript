'use strict'
const express = require('express');
const Controller = require('../controllers/controllers.js');
const controller = new Controller();
const router = express.Router({ caseSensitive: true });

router.get('/home', controller.home);
router.post('/create', controller.create);

module.exports = router;