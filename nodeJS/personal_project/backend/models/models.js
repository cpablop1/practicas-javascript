const Connection = require('../config/connection.js');
const connect = new Connection();

class Model {
    constructor() {
        this.conn = connect.connect();
        this.db = connect.db();
    }
    async create(data) {
        await this.conn;
        return new Promise((resolve, reject) => {
            const sql = `INSERT INTO peopleinfo (first_name, last_name, phone, email)
                            VALUES (?, ?, ?, ?)`;
            const values = [data.first_name, data.last_name, data.phone, data.email];
            this.db.query(sql, values, (err, result) => {
                if (err) return reject('Hubo un error al crear registro!')
                return resolve('Registro creado exitosamente...')
            });
        });
    }
}

module.exports = Model;