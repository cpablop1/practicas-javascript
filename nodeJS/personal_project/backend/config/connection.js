'use strict'
const mysql = require('mysql');

class Connection {
    constructor() {
        this.connection = mysql.createConnection({
            host: 'localhost',
            user: 'root',
            password: '',
            database: 'nodejs'
        });
    }
    connect() {
        return new Promise((resolve, reject) => {
            this.connection.connect(error => {
                if (error) reject(error);
                resolve();
            });
        });
    }
    db = () => {
        return this.connection;
    }
}

module.exports = Connection;

