'use strict'

window.addEventListener('load', () => {
    getListUser().then(data => {
        return data.json();
    }).then(data => {
        listUser(data);
        //return getSingleUser();
        return getInfo();
    }).then(pf=>{
        console.log(pf);
        return getSingleUser();
    }).then(user=>{
        return user.json();
    }).then(user=>{
        listSingleUser(user);
    });
});

function getListUser() {
    return fetch('https://jsonplaceholder.typicode.com/users');
}

function getSingleUser(){
    return fetch('https://reqres.in/api/users/2');
}

function listSingleUser(user){
    let singleUser = document.querySelector('.single-user');
    let img = document.createElement('img');
    let name = document.createElement('b');
    let email = document.createElement('span');
    img.src = user.data.avatar;
    name.innerHTML = `${user.data.first_name} ${user.data.last_name}`;
    email.innerHTML = user.data.email;
    img.style = 'display: block;';
    name.style = 'display: block;'; 
    singleUser.appendChild(img);
    singleUser.appendChild(name);
    singleUser.appendChild(email);
    console.log(user.data);
}

function listUser(users) {
    let table = document.querySelector('table > tbody');
    console.log(users);
    users.map(e => {
        let row = table.insertRow();
        let order = ['id', 'name', 'username', 'phone', 'email', 'address', 'company', 'website'];
        for (let key of order) {
            let cell = row.insertCell();
            if (key === 'address') {
                cell.innerHTML = e[key].city;
            } else if (key === 'company') {
                cell.innerHTML = e[key].name;
            } else {
                cell.innerHTML = e[key];
            }
        }
        /* let row = table.insertRow()
        let cell0 = row.insertCell(0)
        let cell1 = row.insertCell(1)
        let cell2 = row.insertCell(2)
        let cell3 = row.insertCell(3)
        let cell4 = row.insertCell(4)
        let cell5 = row.insertCell(5)
        let cell6 = row.insertCell(6)
        let cell7 = row.insertCell(7)
        cell0.innerHTML = e.id;
        cell1.innerHTML = e.name;
        cell2.innerHTML = e.username;
        cell3.innerHTML = e.phone;
        cell4.innerHTML = e.email;
        cell5.innerHTML = e.address.city;
        cell6.innerHTML = e.company.name;
        cell7.innerHTML = e.website;
        table.appendChild(row); */
    });
}

// Funcion para crear una promesa
function getInfo(){
    let student = {
        name: 'Pablo Pérez',
        phone: '32375649',
        email: 'pablo@gmail.com',
        status: true
    }
    return new Promise((resolve,reject) => {
        let student_string = '';
        setTimeout(() => {
            student_string = JSON.stringify(student);
            if(typeof student_string != 'string' || student_string == '') return reject('error');
            return resolve(student_string);
        }, 3000);
    });
}