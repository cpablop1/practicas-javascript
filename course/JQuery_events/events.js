$(document).ready(()=>{
    console.log('Page loaded...');
    // Mouseover & mouseout
    let box = $('#box');
    /* box.mouseover(e=>{
        $(e.target).css('background-color','red');
    });

    box.mouseout(e=>{
        $(e.target).css('background-color','skyblue');
    }); */

    // Hover
    box.hover(e=>{
        $(e.target).css('background-color','red');
    },e=>{
        $(e.target).css('background-color','skyblue');
    });

    // Click & doble click
    box.click(e=>{
        $(e.target).css('background-color','blue').css('color','white');
    });
    box.dblclick(e=>{
        $(e.target).css('background-color','green');
    });

    // Blur & focus
    let my_input = $('#my_input');
    let data = $('#data')
    my_input.focus(e=>{
        $(e.target).css('border','5px solid green');
    });
    my_input.blur(e=>{
        $(e.target).css('border','5px solid black');
        data.text($(e.target).val()).show();
    });

    // Mousedown & mouseup
    data.mousedown(e=>{
        $(e.target).css('border-color','orange');
    });
    data.mouseup(e=>{
        $(e.target).css('border-color','purple');
    });

    // mousemove
    $(document).mousemove(e=>{
        let follow_me = $('#follow_me')
        follow_me.css('left',e.clientX);
        follow_me.css('top',e.clientY);
    });
});