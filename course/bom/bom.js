'use strict'

function getBom(){ // Esta función permite obtener las dimensiones de la ventana
                    // del navegador (Altura y Anchura)
    //Imprime el ancho y altura de la ventana de visualizacion sin incluir sin
    //incluir barras de desplazamiento, bordes y otras barras de herramientas del navegador.
    console.log(window.innerHeight);
    console.log(window.innerWidth);

    // Por otro lado, estos es todo lo contrario de las anteriores
    console.log(window.outerHeight);
    console.log(window.outerWidth);

    // Estas dos tiene la misma función que outerHeight y outerWidth
    // Extraen el tamaño total de la ventana del navegador incluyendo
    // barras de desplazamiento, bordes y otras barras de herramientas del navegador.
    console.log(screen.height);
    console.log(screen.width);

    console.log('\n' + window.location.href) // Ésta línea imprime la dirección donde se ejecuta este código.
    // Entoces para hacer una redirección basta con escribir el siguiente código:
    // window.location.href = url
    // y para abrir url en nueva pestaña basta con escribir
    // window.open(url)
}

getBom();