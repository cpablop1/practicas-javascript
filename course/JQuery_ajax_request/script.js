$(document).ready(() => {
    console.log('Loaded page...');
    // Load method
    /* $('#data').load('https://reqres.in/'); */
    // Get & Post method
    //$.get('https://reqres.in/api/users', { page: 1 }, response => {
    $.get('https://reqres.in/api/users', response => {
        console.log(response.data);
        response.data.map((value, index) => {
            let data = $('#data');
            let p = document.createElement('p');
            p.textContent = `${value.first_name} ${value.last_name}`;
            data.append(p);
        });
    });


    $('#form_user').submit(e => {
        let name = $(e.target.elements.name);
        let email = $(e.target.elements.email);
        let user = {
            name: name.val(),
            email: email.val()
        }
        $.post('https://reqres.in/api/users', user, response => {
            console.log(response);
            e.target.reset();
        });
        // Segunda forma de hacer peticiones
        $.ajax({
            type: 'POSt',
            url: 'https://reqres.in/api/users',
            data: user,
            beforeSend: ()=>{
                console.log('Sending user...');
            },
            success: response=>{
                console.log(response);
            },
            error: response=>{
                console.log('A shipping error has occurred...!');
            },
            timeout: 2000
        });
        e.preventDefault();
    });
});