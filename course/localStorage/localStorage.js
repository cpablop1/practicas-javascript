'use strict'

// Comprobar compatibilidad del localStorage
if(typeof(Storage) != 'undefined'){
    console.log('LocalStorage disponible!');
}else{
    console.log('LocalStorage no compatible');
}

// Guardar datos en localStorage
localStorage.setItem('title', 'localStorage modified...😁');

// Recuperar datos del local...
console.log(localStorage.getItem('title'));
document.querySelector('#locale').innerText = localStorage.getItem('title');

// Guardar objetos en local...
let user = {
    name: 'Pablo Pérez',
    email: 'pablo@gmail.com' ,
    country: 'Guatemala'
};

localStorage.setItem('user', JSON.stringify(user));

// Recuperar objeto en el local...
console.log(JSON.parse(localStorage.getItem('user')));