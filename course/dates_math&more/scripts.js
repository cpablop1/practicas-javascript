
function clock(){
    const month = ['enero','febrero','marzo','abril','mayo','junio','julio','agosto','septiembre','octubre','noviembre','diciembre'];
    const day = ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'];
    setInterval(()=>{
        let clock = document.querySelector('.clock');
        let random = document.querySelector('.random');
        let date = new Date();
        let hour = document.createElement('h1');
        let time = document.createElement('h3');
        hour.innerHTML = `${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`;
        time.innerHTML = `${day[date.getDay()]} ${date.getDate()} de ${month[date.getMonth()]} de ${date.getFullYear()}`;
        clock.innerHTML = '';
        random.innerHTML = `Number random: ${Math.ceil(Math.random()*100)}`;
        clock.appendChild(time);
        clock.appendChild(hour);

    }, 1000);
}

clock();