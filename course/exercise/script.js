'use strict'

window.addEventListener('load', () => {
    console.log('DOM cargado...');

    let formulario = document.getElementById('formulario');
    let dashed = document.querySelector('.dashed')
    formulario.addEventListener('submit', (e) => {
        let ul = document.createElement('ul');
        let validForm = [];
        let valid = false;
        let errors = [];
        for (let item of e.target.elements) {
            if (item.type === 'text') {
                if (item.value.length === 0) {
                    validForm.push(false);
                    errors.push(`Campo ${item.name} vacío.`);
                } else {
                    let li = document.createElement('li');
                    li.textContent = `${item.name.replace(/\b\w/g, l => l.toUpperCase())}: ${item.value}`;
                    ul.append(li);
                    validForm.push(true);
                }
            }
        }
        valid = validForm.every((item) => item === true);
        if (valid) {
            dashed.append(ul);
            formulario.reset();
        } else {
            alert(errors.join('\n'))
        }

        e.preventDefault();
    });
});

function inputValid(input) {
    input.setCustomValidity(`Campo ${input} obligatorio.`);
}