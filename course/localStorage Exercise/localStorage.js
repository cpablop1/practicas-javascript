'use strict'

// Evento para agregar películas
let formAdd = document.querySelector('#formmovie');
formAdd.addEventListener('submit', (e) => {
    e.preventDefault(); // Colocamos el evento por defecto para evitar recargar la página
    let input = e.target.idmovie; // Accedemos a su campo input
    if (input.value.trim().length !== 0) { // Comprobamos si el campo está vacio
        if (localStorage.getItem('movies') === null) { // Camprobramos si existe un objecto movies en localStorage
            localStorage.setItem('movies', [input.value.trim()]); // Si no existe no creamos
        } else {
            let get = localStorage.getItem('movies').split(','); // si existe, lo extraemos y lo canvertimos en lista
            get.push(input.value.trim()); // la lista extraida le añadimos el valor de input
            localStorage.setItem('movies', get); // despues lo volvemos agregar al localStorage con el nuevo dato
        }
    } else {
        alert('Campo vacío...');
    }
    document.querySelector('#movies').innerHTML = ''; // vaciamos el div movies
    let ul = document.createElement('ul'); // creamos una etiqueta HTML de lista desordenada
    for (let item of localStorage.movies.split(',')) { //Recorremos el objeto movies en el local storage
        let li = document.createElement('li'); // creamos un elemento de la lista
        li.append(item) // agregamos el elemento a la lista
        ul.append(li); // se lo agregamos al elemento ul
    }
    document.querySelector('#movies').appendChild(ul); // y finalmente lo agregamos al div movies
});

// Evento para eliminar películas
let formDelete = document.querySelector('#formMovieDelete');
formDelete.addEventListener('submit', (e) => {
    e.preventDefault(); // Colocamos el evento por defecto para evitar recargar la página
    let input = e.target.idMovieDelete; // Accedemos a su campo input
    if (input.value.trim().length !== 0) { // Comprobamos si el campo está vacio
        if (localStorage.getItem('movies') === null) { // Camprobramos si existe un objecto movies en localStorage
            alert('Listado de películas vacías...')
        } else {
            let get = localStorage.getItem('movies').split(','); // si existe, lo extraemos y lo canvertimos en lista
            let index = get.indexOf(input.value.trim()); // Obtenemos el indice del elemento buscado
            if (index === -1) { // si en el caso no existir
                alert(`No existe ninguna película en la lista con el nombre "${input.value.trim()}"`);
            } else if (get.length === 1) { // si existe, evaluamos si es el unico elemento
                localStorage.removeItem('movies');
            } else {
                get.splice(index, 1); // sino, elinamos el elemento segun el indice
                localStorage.setItem('movies', get); // y lo volvemos agregar la local storage con los datos actualizados
            }
        }
    } else {
        alert('Escribe el nombre de la película a borrar...');
        input.focus();
    }
    document.querySelector('#movies').innerHTML = ''; // vaciamos el div movies
    let ul = document.createElement('ul'); // creamos una etiqueta HTML de lista desordenada
    if (localStorage.getItem('movies') !== null) {
        for (let item of localStorage.movies.split(',')) { //Recorremos el objeto movies en el local storage
            let li = document.createElement('li'); // creamos un elemento de la lista
            li.append(item) // agregamos el elemento a la lista
            ul.append(li); // se lo agregamos al elemento ul
        }
        document.querySelector('#movies').appendChild(ul); // y finalmente lo agregamos al div movies
    }
});