'use strict'

let btn = document.getElementById('clickme'); // Establecemos una variable btn para guardar
                                            // el objecto HTML button
// el objeto btn llamamos su evento click para pasarle la funcion start_event como referencia
btn.addEventListener('click', startEvent);

function startEvent(){ // Y esta es la funcion que muestra una alerta...
    alert('Hola mundo...')
}

/* Keyboard events */
let input = document.getElementById('input_text');
// Focus ---> Se ejecuta cuando se se enfoca algun objeto html ObjectHTML.addEvent('focus', Function callback);
input.addEventListener('focus', (e) => {
    console.log('The input object is focused');
});
// Blur  ---> Se ejecuta cuando se desenfoca algun objeto html ObjectHTML.addEvent('blur', Function callback);
input.addEventListener('blur', () => {
    console.log('The input object is out of focus');
});
// Keydown -> Se ejecuta cuando se preseiona una tecla sobre algun objeto html ObjectHTML.addEvent('keydown', Function callback);
input.addEventListener('keydown', (e) => {
    console.log(`You have pressed the key "${e.key}"`);
});
// Keypress-> Se ejecuta cuando se mantiene presionada una tecla sobre algun objeto html ObjectHTML.addEvent('keypress', Function callback);
input.addEventListener('keypress', (e) => {
    console.log(`You are pressing the key "${e.key}"`);
});
// Keyup ---> Se ejecuta cuando se deja de presionar alguna tecla sobre algun objeto html ObjectHTML.addEvent('keyup', Function callback);
input.addEventListener('keyup', (e) => {
    console.log(`You have stopped pressing the key "${e.key}"`);
});


/* Load event */

window.addEventListener('load', () => { // Este evento se ejecuta cuando se halla cargado todo el html
    console.log('JavaScript loaded...');
});

/* Time functions */

window.addEventListener('load', () => {
    //Timer - Se fragmento de codigo se ejecutará cada cierto tiempo
    setInterval(()=>{
        let title = document.getElementById('title');
        console.log('Code executed...');
        if(title.classList.contains('title')){
            title.classList.remove('title');
            title.innerText = 'Events in JavaScript'
        }else{
            title.classList.add('title')
            title.innerText = 'Events in JavaScript |'
        }

    }, 500); // En este caso se ejecutará cada medio segundo
});

 /*
 /* setInterval se ejecuta en a cada cierto tiempo segun se le indica
 /* y setTimeout se ejecuta una sola vez despues del tiempo que se le indica.
 */

let after = document.getElementById('after');
after.addEventListener('click', ()=> {
    setTimeout(() => {
        alert('Este codigo se ejecuta 3 segundos despues de ser llamada...')
    }, 3000);
});