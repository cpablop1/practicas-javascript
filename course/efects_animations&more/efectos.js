$(document).ready(()=>{
    console.log('Hello world...');
    let box = $('#box');
    $('#show').hide();
    $('#show').click(e=>{
        $(e.target).hide();
        $('#hide').show();
        //box.show('slow','linear');
        box.fadeIn('slow','linear', ()=>{
            console.log('Showing box...');
        });
    });
    $('#hide').click(e=>{
        $(e.target).hide();
        $('#show').show();
        //$('#box').hide('slow','linear');
        box.fadeOut('slow','linear', ()=>{
            console.log('Hiding box...');
        });
    });

    $('#all_in_one').click(e=>{
        box.toggle('slow');
    });

    $('#animation').click(e=>{
        box.animate({
            marginLeft: '500px',
            fontSize: '40px',
            height: '110px'
        },'slow').animate({
            borderRadius: '900px',
            marginTop: '200px'
        },'slow').animate({
            borderRadius: '0px',
            marginLeft: '0px'
        },'slow').animate({
            borderRadius: '100px',
            marginTop: '0px'
        },'slow').animate({
            marginLeft: '500px',
            fontSize: '40px',
            height: '110px'
        },'slow');
    });
});