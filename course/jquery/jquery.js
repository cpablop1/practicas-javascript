$(document).ready(()=>{
    console.log('Using JQuery, thats great!');

    $('#red').css('color', 'red');
    $('#yellow').css('color', 'yellow');
    $('#green').css('color', 'green');

    $('.dashed').css('border','5px dashed grey');

    let p = $('p');

    p.click((e)=>{
        let that = $(e.target); 
        if(that.hasClass('big-text')){
            that.removeClass('big-text');
        }else{
            that.addClass('big-text');
        }
    });

    /// Etiquetas por atributos...
    let input_text = $('[type="text"]');
    let input_number = $('[type="number"]');
    input_text.css('background', 'grey').css('color','white');
    input_number.css('background','skyblue').css('color','white');
    console.log(input_text);
    console.log(input_number);
});