> # PRACTICAS JAVASCRIPT ![](https://i.pinimg.com/originals/16/ce/87/16ce875b4340daf93b9e65ef7d960d03.png){width=100}

[![](https://cdn-icons-png.flaticon.com/512/5678/5678732.png){width=50} más info...](https://developer.mozilla.org/es/docs/conflicting/Web/JavaScript)

JavaScript (a menudo abreviado como JS) es un lenguaje ligero, interpretado y orientado a objetos con funciones de primera clase, y mejor conocido como el lenguaje de programación para las páginas Web, pero también se utiliza en muchos entornos que no son de navegador. Es un lenguaje de scripts que es dinámico, multiparadigma, basado en prototipos y admite estilos de programación orientados a objetos, imperativos y funcionales.

JavaScript se ejecuta en el lado del cliente de la web, y se puede utilizar para estilizar/programar cómo se comportan las páginas web cuando ocurre un evento. JavaScript es un potente lenguaje de scripts y fácil de aprender, ampliamente utilizado para controlar el comportamiento de las páginas web.

Contrariamente a la creencia popular, JavaScript no es "Java interpretado". En pocas palabras, JavaScript es un lenguaje de scripts dinámico que admite construcción de objetos basada en prototipos. Intencionalmente, la sintaxis básica es similar a Java y C++ para reducir la cantidad de conceptos nuevos necesarios para aprender el lenguaje. Construcciones del lenguaje, como las declaraciones `if`, los bucles `for` y `while`, y `switch` y los bloques `try...catch` funcionan igual que en esos lenguajes (o casi).

JavaScript puede funcionar como un lenguaje orientado a objetos (en-US) y procedimental. Los objetos se crean mediante programación en JavaScript, adjuntando métodos y propiedades a objetos que de otro modo en tiempo de ejecución estarían vacíos, a diferencia de las definiciones de clases sintácticas comunes en lenguajes compilados como C++ y Java. Una vez que se ha construido un objeto, se puede utilizar como plano (o prototipo) para crear objetos similares.

Las capacidades dinámicas de JavaScript incluyen la construcción de objetos en tiempo de ejecución, listas de parámetros variables, variables de función, creación dinámica de scripts (a través de `eval`, introspección de objetos (a través de `for...in`) y recuperación de código fuente (los programas JavaScript pueden descompilar los cuerpos de las funciones en su texto fuente).
